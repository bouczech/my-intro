import { CanDeactivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { ProjectEditComponent } from '../projects/project-edit/project-edit.component';
import { AlertifyService } from '../services/alertify.service';

@Injectable()
export class PreventUnsavedChanges implements CanDeactivate<ProjectEditComponent> {
    constructor(private alertify: AlertifyService) { }

    canDeactivate(component: ProjectEditComponent) {
        if (component.projectForm.dirty) {
            return confirm('Are you sure? Unsaved data will be lost!');
        }
        return true;
    }
}