import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptorService } from './services/auth-interceptor.service';
import { ApplicationDetailResolver } from './resolvers/application-detail.resolver';
import { PreventUnsavedChanges } from './guard/prevent-unsaved-changes.guard';

@NgModule({
    providers: [
        {// u interceptosrs neni jina moznost nez to providovat zde
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptorService,
            multi: true
        },
        ApplicationDetailResolver,
        PreventUnsavedChanges
    ]
})
export class CoreModule { }