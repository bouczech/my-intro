export class Project {
  public name: string;
  public desc: string;
  public imagePath: string;
  public filePath: string;
  public lastUpdate: Date;

  constructor(name: string, desc: string, imagePath: string, filePath: string, lastUpdate: Date) {
    this.name = name;
    this.desc = desc;
    this.imagePath = imagePath;
    this.filePath = filePath;
    this.lastUpdate = lastUpdate;
  }
}
