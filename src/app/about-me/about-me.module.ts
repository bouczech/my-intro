import { NgModule } from '@angular/core';
import { AboutMeComponent } from './about-me.component';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        AboutMeComponent
    ],
    imports: [
        RouterModule.forChild([{ path: '', component: AboutMeComponent }])
    ]
})
export class AboutMeModule { }