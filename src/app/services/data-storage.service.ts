import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

import { Project } from '../models/project.model';
import { ProjectService } from './project.service';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {

  constructor(private http: HttpClient, private projectService: ProjectService) { }

  loadProjects() {
    return this.http.get<Project[]>('https://my-intro-boucek.firebaseio.com/project.json').pipe(
      tap(apps => {
        this.projectService.setProjects(apps);
      })
    );
  }

  storeProjects() {
    const apps = this.projectService.getProjects();
    console.log("Saved data!!");
    this.http.put('https://my-intro-boucek.firebaseio.com/project.json', apps)
      .subscribe(response => {
      });
  }

}
