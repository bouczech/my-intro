import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Project } from '../models/project.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {


  // private apps: Project[] = [
  //   new Project(
  //     'Flappy Game',
  //     'Jumping game which everybody loves',
  //     'https://github.com/bouceka/unity-games/raw/master/images/player.png',
  //     'https://github.com/bouceka/unity-games/blob/master/Flappy/Falppy%201.1.2.zip',
  //     new Date()),
  //   new Project(
  //     'Run Game',
  //     'Run game which everybody loves',
  //     'https://github.com/bouceka/unity-games/raw/master/images/Icon%20(2).jpg',
  //     'https://github.com/bouceka/unity-games/blob/master/RunGame/RunGame0.0.0.apk',
  //     new Date())
  // ];

  projectsChanged = new Subject<Project[]>();
  private projects: Project[] = [];

  constructor() { }

  getProjects() {
    return this.projects.slice();
  }

  getProject(id: number) {
    return this.projects[id];
  }

  setProjects(projects: Project[]) {
    this.projects = projects || [];
    this.projectsChanged.next(this.projects.slice());
  }

  addProject(projects: Project) {
    this.projects.push(projects);
    this.projectsChanged.next(this.projects.slice());
  }

  deleteProject(id: number) {
    this.projects.splice(id, 1);
    this.projectsChanged.next(this.projects.slice());
  }

  updateProject(index: number, editedApp: Project) {
    this.projects[index] = editedApp;
    this.projectsChanged.next(this.projects.slice());
  }




}


