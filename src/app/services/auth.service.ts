import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators'
import { throwError, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';
import { AlertifyService } from './alertify.service';


export interface AuthResponse {
    idToken: string;
    email: string;
    refreshToken: string;
    expiresIn: string;
    localId: string;
    registred?: boolean;
}

@Injectable({ providedIn: 'root' })

export class AuthService {

    user = new BehaviorSubject<User>(null); // aktualni instance usera v aplikaci
    private tokenExpirationTimer: any;

    constructor(private http: HttpClient, private router: Router) { }

    signUp(email: string, password: string) {

        return this.http
            .post<AuthResponse>('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=' + environment.firebaseAPIKey,
                {
                    email: email,
                    password: password,
                    returnSecureToken: true
                }).pipe(catchError(this.handleError),
                    tap(response => {
                        this.handleAuth(response.email,
                            response.localId,
                            response.idToken,
                            +response.expiresIn);
                        
                    }));
    }

    signIn(email: string, password: string) {
        return this.http
            .post<AuthResponse>('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=' + environment.firebaseAPIKey,
                {
                    email: email,
                    password: password,
                    returnSecureToken: true
                }
            ).pipe(catchError(this.handleError),
                tap(response => {
                    this.handleAuth(response.email,
                        response.localId,
                        response.idToken,
                        +response.expiresIn);
                }));
    }

    private handleAuth(email: string, id: string, token: string, expiresIn: number) {
        const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);
        const authUser = new User(email, id, token, expirationDate);
        this.user.next(authUser);
        this.autoLogout(expiresIn * 1000);
        localStorage.setItem('user', JSON.stringify(authUser));
    }

    logout() {
        this.user.next(null);
        this.router.navigate(['/auth']);
        localStorage.removeItem('user');
        if (this.tokenExpirationTimer) {
            clearTimeout(this.tokenExpirationTimer);
        }
        this.tokenExpirationTimer = null;
    }

    private autoLogout(expiration: number) {
        this.tokenExpirationTimer = setTimeout(() => {
            this.logout();
        }, expiration);
    }

    autoLogin() {
        const userData: {
            email: string;
            id: string;
            _token: string;
            _tokenExpirationDate: Date;
        } = JSON.parse(localStorage.getItem('user'));
        if (!userData) {
            return;
        }
        const loadedUser = new User(
            userData.email,
            userData.id,
            userData._token,
            new Date(userData._tokenExpirationDate));

        if (loadedUser.token) {
            this.user.next(loadedUser);
            const expirationDuration = new Date(userData._tokenExpirationDate).getTime() - new Date().getTime();
            this.autoLogout(expirationDuration);
        }
    }

    private handleError(errorResponse: HttpErrorResponse) {
        let errorMessage = 'An Error';
        if (!errorResponse.error || !errorResponse.error.error) {// neznamy error osetreni 
            return throwError(errorMessage);
        }
        switch (errorResponse.error.error.message) {
            case 'EMAIL_EXISTS':
                errorMessage = 'This email exists already';
                break;
            case 'EMAIL_NOT_FOUND':
                errorMessage = 'This email does not exist.';
                break;
            case 'INVALID_PASSWORD':
                errorMessage = 'This passwors is invalid.';
                break;
        }
        return throwError(errorMessage);
    }

}
