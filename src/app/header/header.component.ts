import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, NgForm } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';


import { AuthService, AuthResponse } from '../services/auth.service';
import { DataStorageService } from '../services/data-storage.service';
import { AlertifyService } from '../services/alertify.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  private userSub: Subscription;
  isAuth = false;
  signupForm: FormGroup;
  error = null;
  navbarOpen = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private alertify: AlertifyService) { }

  ngOnInit() {
    this.userSub = this.authService.user.subscribe(user => {
      this.isAuth = !!user;
    });
  }

  onToggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  isAuthRoute() {
    return this.router.url === '/auth';
  }

  onLogout() {
    this.authService.logout();
    this.alertify.warning('Logged Out');
  }

  onSubmit(authForm: NgForm) {
    if (!authForm.valid) {
      return;
    }
    const email = authForm.value.email;
    const password = authForm.value.password;

    let authObs: Observable<AuthResponse>;

    authObs = this.authService.signIn(email, password);
    authObs.subscribe(() => {
      this.alertify.success('Signed Up Successfully');
      this.router.navigate(['/application']);
    }, errorMessage => {
      this.error = errorMessage;
    });
    authForm.reset();
  }

  onHandleError() {
    this.error = null;
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }

}
