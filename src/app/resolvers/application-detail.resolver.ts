import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterState, RouterStateSnapshot, Router } from '@angular/router';

import { DataStorageService } from '../services/data-storage.service';
import { ProjectService } from '../services/project.service';
import { Project } from '../models/project.model';



@Injectable()
export class ApplicationDetailResolver implements Resolve<Project[]> {
    constructor(
        private dataStorageService: DataStorageService,
        private projectService: ProjectService,
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
     
        const apps = this.projectService.getProjects();
        if (apps.length === 0) {
            return this.dataStorageService.loadProjects();
        }
        return apps;
    }
}