import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';

import { AuthGuard } from '../guard/auth.guard';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { ApplicationDetailResolver } from '../resolvers/application-detail.resolver';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { PreventUnsavedChanges } from '../guard/prevent-unsaved-changes.guard';

const myAppRoutes: Routes = [
    {
        path: '',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuard],
        children: [
            { path: '', component: ProjectListComponent },
            { path: 'new', component: ProjectEditComponent, resolve: [ApplicationDetailResolver] },
            { path: ':id', component: ProjectDetailComponent, resolve: [ApplicationDetailResolver] },
            {
                path: ':id/edit', component: ProjectEditComponent,
                resolve: [ApplicationDetailResolver],
                canDeactivate: [PreventUnsavedChanges]
            }]
    },
];

@NgModule({
    imports: [RouterModule.forChild(myAppRoutes)],
    exports: [RouterModule]
})

export class ProjectRoutingModule {}