import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';

import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project.model';
import { DataStorageService } from 'src/app/services/data-storage.service';
import { AlertifyService } from 'src/app/services/alertify.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {

  project: Project;
  id: number;

  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService,
    @Inject(DOCUMENT) private document: Document,
    private router: Router,
    private dataStorageService: DataStorageService,
    private alertify: AlertifyService) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.project = this.projectService.getProject(this.id);
      });
  }

  onGoToUrl(): void {
    this.document.location.href = this.project.filePath;
  }

  onDeleteProject() {
    this.projectService.deleteProject(this.id);
    this.dataStorageService.storeProjects();
    this.alertify.warning("Deleted Successfully");
    this.router.navigate(['/application'], { relativeTo: this.route });
  }

  onEditProject() {
    this.router.navigate(['../', this.id, 'edit'], { relativeTo: this.route });
  }

  onConfirmDelete() {
    this.alertify.confirm('Are you sure?', () => { this.onDeleteProject(); });
  }


}
