import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { Project } from 'src/app/models/project.model';
import { ProjectService } from 'src/app/services/project.service';
import { DataStorageService } from 'src/app/services/data-storage.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit, OnDestroy {

  projects: Project[];
  isLoading = false;
  subscription: Subscription;

  constructor(
    private projectService: ProjectService,
    private dataStorageService: DataStorageService,
    private router: Router,
    private route: ActivatedRoute) { }


  ngOnInit() {
    this.isLoading = true;
    this.subscription = this.projectService.projectsChanged.subscribe((projects: Project[]) => {
      this.projects = projects;
      this.dataStorageService.storeProjects();
    });
    this.projects = this.projectService.getProjects();
    if (!this.projects.length) {
      this.dataStorageService.loadProjects().subscribe();
    }
    this.isLoading = false;
  }


  onNewApp() {
    this.router.navigate(['new'], { relativeTo: this.route });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }



}
