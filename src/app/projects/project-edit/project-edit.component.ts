import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ProjectService } from 'src/app/services/project.service';
import { AlertifyService } from 'src/app/services/alertify.service';
import { DataStorageService } from 'src/app/services/data-storage.service';

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.css']
})
export class ProjectEditComponent implements OnInit {

  isEditMode = false;
  projectForm: FormGroup;
  id: number;

  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (this.projectForm.dirty) {
      $event.returnValue = true;
    }
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private alertify: AlertifyService,
    private projectService: ProjectService,
    private dataStorageService: DataStorageService
  ) { }

  ngOnInit() {
    this.route.params.subscribe((param: Params) => {
      this.id = +param['id'];
      this.isEditMode = param['id'] != null;
      this.initForm();
    });


  }

  onSubmit() {
    if (!this.isEditMode) {
      this.projectService.addProject(this.projectForm.value);
      this.onCancel();
    } else {
      this.projectService.updateProject(this.id, this.projectForm.value);
      this.projectForm.reset(this.projectForm.value);
    }
    this.dataStorageService.storeProjects();
    this.alertify.success('Saved successfully');
  }

  onCancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  // REACTIVE APPROACH
  private initForm() {
    let appName = '';
    let appDesc = '';
    let imgPath = '';
    let appPath = '';
    let lastUpdate = new Date();
    if (this.isEditMode) {
      const app = this.projectService.getProject(this.id);
      appName = app.name;
      appDesc = app.desc;
      imgPath = app.imagePath;
      appPath = app.filePath;
    }
    this.projectForm = new FormGroup({
      'name': new FormControl(appName, Validators.required),
      'imagePath': new FormControl(imgPath, Validators.required),
      'desc': new FormControl(appDesc, Validators.required),
      'filePath': new FormControl(appPath, Validators.required),
      'lastUpdate': new FormControl(lastUpdate)
    });

  }
}
