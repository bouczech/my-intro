import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectCardComponent } from './project-card/project-card.component';
import { ProjectRoutingModule } from './project-routing.module';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    declarations: [
        ProjectDetailComponent,
        ProjectEditComponent,
        ProjectListComponent,
        ProjectCardComponent
    ],
    imports: [
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        ProjectRoutingModule,
        TabsModule.forRoot(),
        SharedModule

    ]
})
export class ProjectModule { }