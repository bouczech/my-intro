import { Component, AfterViewInit, ElementRef, OnInit } from '@angular/core';

import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit, OnInit {
  constructor(private elementRef: ElementRef, private authService: AuthService) {

  }
  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = ' #f4f6f9';
  }

  ngOnInit() {
    this.authService.autoLogin();
  }
}
